type Template = {
  id: string;
  superId: string | null;
  title: string;
  iterationLogic: string;
  resolutionLogic: string;
};

type Skeleton = {
  id: string;
  superId?: string;
  title?: string;
};

type Instance = {
  id: string;
  templateId: string;
  superId: string | null;
  title: string | null;
};

type Resolution = {
  id: string;
  templateId: string;
  superId: string | null;
  resolution: boolean | number | string;
};

type InstanceOrResolution = {
  id: string;
  templateId: string;
  superId: string | null;
  title?: string | null;
  resolution?: string | number | boolean;
};
