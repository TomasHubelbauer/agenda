export const once = (id: string) => `
yield ({ id: '${id}' });
`.trim();

export const daily = `
let last;
for (const day of days(fromDate, toDate)) {
  last = { id: day.toISOString().substring(0, 10), superId: last ? last.id : null, title: template.title };
  yield last;
}
`.trim();

// Rename to dependent as this follows whatever rhythm the principal sets up
export const dailyDependent = `
for (const instance of instances(template.superId, fromDate, toDate)) {
  yield ({ id: instance.id, superId: instance.id, title: template.title });
}
`.trim();

export const weekly = `
const instant = new Date();
instant.setUTCDate(instant.getUTCDate() + 4 - (instant.getUTCDay() || 7));
const weekNo = Math.ceil((((Date.now() - new Date(Date.UTC(new Date().getUTCFullYear(), 0, 1)).getTime()) / 86400000) + 1) / 7);
yield ({ id: instant.getFullYear() + '-' + weekNo });
`.trim();

export const monthly = `
yield ({ id: new Date().toISOString().substring(0, 7) });
`.trim();
