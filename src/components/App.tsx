import * as React from 'react';
import store from '../store';
import Modal from './modals/Modal';
import CreateTemplateModal, { CreateTemplateModalProps } from './modals/CreateTemplateModal';
import MoveTemplateModal, { MoveTemplateModalProps } from './modals/MoveTemplateModal';
import EditTemplateIterationLogicModal, { EditTemplateIterationLogicModalProps } from './modals/EditTemplateIterationLogicModal';
import EditTemplateResolutionLogicModal, { EditTemplateResolutionLogicModalProps } from './modals/EditTemplateResolutionLogicModal';
import { flow, generateInstances, iterateSkeletons } from '../logic';

type CreateTemplateModalRequest = { type: typeof CreateTemplateModal.type, props: CreateTemplateModalProps };
type MoveTemplateModalRequest = { type: typeof MoveTemplateModal.type, props: MoveTemplateModalProps };
type EditTemplateIterationLogicModalRequest = { type: typeof EditTemplateIterationLogicModal.type, props: EditTemplateIterationLogicModalProps };
type EditTemplateResolutionLogicModalRequest = { type: typeof EditTemplateResolutionLogicModal.type, props: EditTemplateResolutionLogicModalProps };
type ModalType =
    | typeof CreateTemplateModal.type
    | typeof MoveTemplateModal.type
    | typeof EditTemplateIterationLogicModal.type
    | typeof EditTemplateResolutionLogicModal.type
    ;
type ModalRequest = 
    | CreateTemplateModalRequest
    | MoveTemplateModalRequest
    | EditTemplateIterationLogicModalRequest
    | EditTemplateResolutionLogicModalRequest
    ;

type AppState = {
  viewDevMode: boolean;
  modals: ModalRequest[];
  fromDate: Date;
  toDate: Date;
  instant: Date;
};
type AppProps = {};

export default class App extends React.Component<AppProps, AppState> {
  private instantInterval: number | undefined;

  constructor(props: AppProps) {
    super(props);
    const fromDate = new Date(new Date().setDate(new Date().getDate()));
    const toDate = new Date(new Date().setDate(new Date().getDate()));
    this.state = {
      // TODO: Load this through Store
      viewDevMode: localStorage.getItem('agenda-dev') === 'dev',
      modals: [],
      fromDate,
      toDate,
      instant: new Date()
    };
  }

  componentDidMount() {
      this.instantInterval = window.setInterval(() => this.setState({ instant: new Date() }), 1000);
  }

  componentWillUnmount() {
      window.clearInterval(this.instantInterval);
  }

  render() {
    return (
      <div>
        {this.state.instant.toLocaleString()}
        <br />
        <button onClick={this.onShowCreateTemplateModalButtonClick}>Create</button>
        {this.renderInstances()}
        <input id='devModeCheckboxInput' type='checkbox' onChange={this.onDevModeInputChange} checked={this.state.viewDevMode} />
        <label htmlFor='devModeCheckboxInput'>Developer mode</label>
        {this.state.viewDevMode && this.renderDevMode()}
        {this.renderModal()}
      </div>
    );
  }

  // TODO: Paginate this as it is heavy on the mobile CPU
  renderDevMode() {
    const templates = store.templates;
    const resolutions = store.resolutions;
    return (
        <div>
            <button onClick={this.onExportButtonClick}>Export</button>
            <button onClick={this.onImportButtonClick}>Import</button>
            <h3>Templates</h3>
            <div>
                {templates.length > 0 ? <button onClick={this.onClearTemplatesButtonClick}>Clear</button> : null}
            </div>
            {templates.map((template, index, array) => {
                const { id, title, superId, iterationLogic, resolutionLogic, ...rest } = template; 
                return (
                    <details key={template.id}>
                        <summary>
                            <h4 style={{ display: 'inline' }}>{template.title} ({template.superId ? <code>{template.superId}</code> : 'root'} -> <code>{template.id}</code>)</h4>
                        </summary>
                        <div>
                            <button data-id={template.id} onClick={this.onRenameTemplateButtonClick}>Rename</button>
                            <button data-id={template.id} onClick={this.onDeleteTemplateButtonClick}>Delete</button>
                            {template.superId !== null && <button data-id={template.id} onClick={this.onShowMoveTemplateModalButtonClick}>Move</button>}
                            <button data-id={template.id} onClick={this.onPreviewTemplateButtonClick}>Preview</button>
                            {index === 0 ? null : <button data-id={template.id} onClick={this.onMoveTemplateUpButtonClick}>↑</button>}
                            {index === array.length - 1 ? null : <button data-id={template.id} onClick={this.onMoveTemplateDownButtonClick}>↓</button>}
                        </div>
                        {Object.keys(rest).length > 0 && <code>{JSON.stringify(rest)}</code>}
                        <h5>Iteration Logic</h5>
                        <button data-id={template.id} onClick={this.onEditTemplateIterationLogicButtonClick}>Edit</button>
                        <pre>{template.iterationLogic}</pre>
                        <h5>Resolution Logic</h5>
                        <button data-id={template.id} onClick={this.onEditTemplateResolutionLogicButtonClick}>Edit</button>
                        <pre>{template.resolutionLogic}</pre>
                    </details>
                );
            })}
            <h3>Resolutions</h3>
            <div>
                {resolutions.length > 0 ? <button onClick={this.onClearInstancesButtonClick}>Clear</button> : null}
            </div>
            {resolutions.map(resolution => {
                const template = templates.find(t => t.id === resolution.templateId);
                if (template === undefined) {
                    throw new Error();
                }

                return (
                    <details key={resolution.id}>
                        <summary>
                            <h4 style={{ display: 'inline' }}>{template.title} (<code>{resolution.id}</code>)</h4>
                        </summary>
                        <div>
                            <button data-id={resolution.id} onClick={this.onDeleteInstanceButtonClick}>Delete</button>
                        </div>
                        <code>{JSON.stringify(resolution)}</code>
                    </details>
                );
            })}
        </div>
    );
  }

  renderInstances() {
    const templates = store.templates;
    const resolutions = store.resolutions;
    const instances = templates.reduce((instances, template) => [...instances, ...generateInstances(template, this.state.fromDate, this.state.toDate)], [] as Instance[]);
    const levels = [];
    const paths = [ ...flow([ ...instances, ...resolutions ], templates) ];
    for (let level = 0; level < Math.max(...paths.map(path => path.length)); level++) {
        const instances = paths.map(path => path[level]).filter(item => item !== undefined).filter((item, index, array) => array.indexOf(item) === index);
        levels.push(
            <div key={level}>
                {instances.map(instance => {
                    const template = templates.find(template => template.id === instance.templateId);
                    if (template === undefined) {
                        throw new Error();
                    }

                    return (
                        <button
                            key={instance.id}
                            data-id={instance.id}
                            data-template-id={instance.templateId}
                            data-super-id={instance.superId}
                            disabled={level !== 0}
                            onClick={this.onInstanceButtonClick}
                        >
                            {instance.title || template.title}
                        </button>
                    );
                })}
            </div>
        );
    }

    return <div>{levels}</div>;
  }

  renderModal() {
    const modal = this.state.modals[this.state.modals.length - 1];
    if (modal === undefined) {
        return;
    }

    switch (modal.type) {
        case CreateTemplateModal.type: return (
            <Modal title="Create template" onCancel={this.onCancelCreateTemplateModal}>
                <CreateTemplateModal {...modal.props} />
            </Modal>
        );
        case MoveTemplateModal.type: return (
            <Modal title="Move template" onCancel={this.onCancelMoveTemplateModal}>
                <MoveTemplateModal {...modal.props} />
            </Modal>
        );
        case EditTemplateIterationLogicModal.type: return (
            <Modal title="Edit template iteration logic" onCancel={this.onCancelEditTemplateIterationLogicModal}>
                <EditTemplateIterationLogicModal {...modal.props} />
            </Modal>
        );
        case EditTemplateResolutionLogicModal.type: return (
            <Modal title="Edit template resolution logic" onCancel={this.onCancelEditTemplateResolutionLogicModal}>
                <EditTemplateResolutionLogicModal {...modal.props} />
            </Modal>
        );
        default: throw new Error(`Unexpected modal type ${(modal /* never */ as { type: string; }).type}`);
    }
  }

  onShowCreateTemplateModalButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const modal: CreateTemplateModalRequest = { type: 'create-template', props: { templates: store.templates, onConfirm: this.onCreateTemplate } };
    this.setState(state => ({ ...state, modals: [ ...state.modals, modal ] }));
  }

  onDevModeInputChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    this.setState({ viewDevMode: event.currentTarget.checked }, () => {
      // TODO: Do this through store
      localStorage.setItem('agenda-dev', this.state.viewDevMode ? 'dev' : '');
    });
  }

  onExportButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const { templates, resolutions } = store;
    const link = document.createElement('a');
    const timestamp = new Date();
    link.download = `agenda-${timestamp.toISOString().substring(0, 19).replace(/[T:]/g, '-')}.json`;
    link.href = 'data:application/json;encoding=utf8,' + encodeURIComponent(JSON.stringify({ timestamp, templates, resolutions }, null, 2));
    document.body.appendChild(link);
    link.click()
    document.body.removeChild(link);
  }

  onImportButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    if (!confirm('Are you sure you want to replace existing data? You can export a backup first.')) {
      return;
    }

    const input = document.createElement('input');
    input.type = 'file';
    input.onchange = (event: Event) => {
        const files = (event.currentTarget as HTMLInputElement).files;
        if (files === null) {
            return;
        }

        if (files.length !== 1) {
            throw new Error('Pick one file');
        }

        const fileReader = new FileReader();
        fileReader.addEventListener('loadend', async event2 => {
            // TODO: Get rid of the instances fallback after migration
            const { timestamp, templates, resolutions, instances } = JSON.parse(fileReader.result);
            store.templates = templates;
            store.resolutions = resolutions || instances;
            alert(`Loaded ${store.templates.length} templates and ${store.resolutions.length} resolutions from ${timestamp} backup!`);
            // Force migration!
            location.reload();
        });

        fileReader.readAsText(files[0]);
    };

    document.body.appendChild(input);
    input.click()
    document.body.removeChild(input);
  }

  onClearTemplatesButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    if (!confirm('Are you sure you want to delete all templates and resolutions?')) {
      return;
    }

    store.resolutions = [];
    store.templates = [];
    this.forceUpdate();
  }

  onRenameTemplateButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const id = event.currentTarget.dataset['id'];
    const templates = store.templates;
    const template = templates.find(template => template.id === id);
    if (template === undefined) {
        throw new Error();
    }

    const title = prompt('Title:', template.title);
    if (!title) {
        return;
    }

    template.title = title;
    store.templates = templates;
    this.forceUpdate();
  }

  *iterateDependentTemplates(templates: Template[], superId: string): IterableIterator<Template> {
    for (const template of templates.filter(template => template.superId === superId)) {
        yield template;
        yield* this.iterateDependentTemplates(templates, template.id);
    }
  }

  onDeleteTemplateButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const resolutions = store.resolutions;
    const templates = store.templates;

    const id = event.currentTarget.dataset['id'];
    const template = templates.find(template => template.id === id);
    if (template === undefined) {
        throw new Error();
    }

    const dependentTemplates = [ template, ...this.iterateDependentTemplates(templates, template.id) ];
    if (!confirm(`Delete templates and their resolutions?\n\n${dependentTemplates.map(t => t.title).join('\n')}`)) {
        return;
    }

    const removedTemplateIds = dependentTemplates.map(template => template.id);

    store.resolutions = resolutions.filter(resolution => !removedTemplateIds.includes(resolution.templateId));
    store.templates = templates.filter(template => !removedTemplateIds.includes(template.id));
    this.forceUpdate();
  }

  onShowMoveTemplateModalButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const templateId = event.currentTarget.dataset['id'];
    if (templateId === undefined) {
        throw new Error();
    }

    const templates = store.templates;
    const template = templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    if (template.superId === null) {
        throw new Error('Cannot move independent templates');
    }

    const modal: MoveTemplateModalRequest = { type: MoveTemplateModal.type, props: { templates, templateId, superId: template.superId, onConfirm: this.onMoveTemplate } };
    this.setState(state => ({ ...state, modals: [ ...state.modals, modal ] }));
  }

  onMoveTemplateUpButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const id = event.currentTarget.dataset['id'];
    const templates = store.templates;
    const template = templates.find(i => i.id === id);
    if (template === undefined) {
        throw new Error();
    }

    const index = templates.indexOf(template);
    if (index === 0) {
        return;
    }

    const prevTemplate = templates[index - 1];
    templates[index - 1] = template;
    templates[index] = prevTemplate;

    store.templates = templates;
    this.forceUpdate();
  }

  onMoveTemplateDownButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const id = event.currentTarget.dataset['id'];
    const templates = store.templates;
    const template = templates.find(i => i.id === id);
    if (template === undefined) {
        throw new Error();
    }

    const index = templates.indexOf(template);
    if (index === templates.length - 1) {
        return;
    }

    const nextTemplate = templates[index + 1];
    templates[index + 1] = template;
    templates[index] = nextTemplate;

    store.templates = templates;
    this.forceUpdate();
  }

  onClearInstancesButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    if (!confirm('Are you sure you want to delete all resolutions?')) {
      return;
    }

    store.resolutions = [];
    this.forceUpdate();
  }

  onDeleteInstanceButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const id = event.currentTarget.dataset['id'];
    const resolutions = store.resolutions;
    const resolutionIndex = resolutions.findIndex(template => template.id === id);
    const resolution = resolutions[resolutionIndex];
    // TODO: Show template title
    if (!confirm(`Delete instance ${resolution.id}?`)) {
        return;
    }

    resolutions.splice(resolutionIndex, 1);
    store.resolutions = resolutions;
    this.forceUpdate();
  }

  onInstanceButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    /* Watch out, `dataset` automatically converts to camelCase: template-id = templateId, templateId = templateid */
    const resolutions = store.resolutions;

    const id = event.currentTarget.dataset['id'];
    if (id === undefined) {
        throw new Error();
    }

    const templateId = event.currentTarget.dataset['templateId'];
    if (templateId === undefined) {
        throw new Error();
    }

    const template = store.templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    const superId = event.currentTarget.dataset['superId'] || null;
    try {
        const resolution = eval(template.resolutionLogic) as string | number | boolean;
        resolutions.push({ id, templateId, resolution, superId });
    } catch (error) {
        alert(`Problem in template ${templateId} resolution logic: ${error.toString()}`);
        return;
    }

    store.resolutions = resolutions;
    this.forceUpdate();
  }

  hideModal(type: ModalType) {
    const modal = this.state.modals[this.state.modals.length - 1];
    if (modal.type !== type) {
        alert(`Unexpected modal type ${modal.type}`);
        throw new Error(`Unexpected modal ${modal.type}`);
    }
    
    this.setState(state => ({ ...state, modals: state.modals.slice(0, -1) }))
  }

  onCancelMoveTemplateModal = () => {
    this.hideModal(MoveTemplateModal.type);
  }

  onCancelCreateTemplateModal = () => {
    this.hideModal(CreateTemplateModal.type);
  }

  onCreateTemplate = (template: Template) => {
    const templates = store.templates;
    templates.push(template);
    store.templates = templates;
    this.hideModal(CreateTemplateModal.type);
  }

  onMoveTemplate = (templateId: string, superId: string) => {
    const templates = store.templates;
    const template = templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    template.superId = superId;
    store.templates = templates;
    this.hideModal(MoveTemplateModal.type);
  }

  onPreviewTemplateButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const template = store.templates.find(template => template.id === event.currentTarget.dataset['id']);
    if (template === undefined) {
        throw new Error();
    }

    alert([ ...iterateSkeletons(template, this.state.fromDate, this.state.toDate) ].map(instance => JSON.stringify(instance)).join('\n'));
  }

  onEditTemplateIterationLogicButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const templateId = event.currentTarget.dataset['id'];
    if (templateId === undefined) {
        throw new Error();
    }

    const template = store.templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    const modal: EditTemplateIterationLogicModalRequest = { type: EditTemplateIterationLogicModal.type, props: { templateId, iterationLogic: template.iterationLogic, onConfirm: this.onEditTemplateIterationLogic } };
    this.setState(state => ({ ...state, modals: [ ...state.modals, modal ] }));
  }

  onEditTemplateResolutionLogicButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    const templateId = event.currentTarget.dataset['id'];
    if (templateId === undefined) {
        throw new Error();
    }

    const template = store.templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    const modal: EditTemplateResolutionLogicModalRequest = { type: EditTemplateResolutionLogicModal.type, props: { templateId, resolutionLogic: template.resolutionLogic, onConfirm: this.onEditTemplateResolutionLogic } };
    this.setState(state => ({ ...state, modals: [ ...state.modals, modal ] }));
  }


  onCancelEditTemplateIterationLogicModal = () => {
    this.hideModal(EditTemplateIterationLogicModal.type);
  }

  onCancelEditTemplateResolutionLogicModal = () => {
    this.hideModal(EditTemplateResolutionLogicModal.type);
  }

  onEditTemplateIterationLogic = (templateId: string, iterationLogic: string) => {
    const templates = store.templates;
    const template = templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    template.iterationLogic = iterationLogic;
    store.templates = templates;
    this.hideModal(EditTemplateIterationLogicModal.type);
  }

  onEditTemplateResolutionLogic = (templateId: string, resolutionLogic: string) => {
    const templates = store.templates;
    const template = templates.find(template => template.id === templateId);
    if (template === undefined) {
        throw new Error();
    }

    template.resolutionLogic = resolutionLogic;
    store.templates = templates;
    this.hideModal(EditTemplateResolutionLogicModal.type);
  }
}
