import * as React from 'react';
import TemplateSelector from '../TemplateSelector';

export type MoveTemplateModalProps = {
    templates: Template[];
    templateId: string;
    superId: string;
    onConfirm: (templateId: string, superId: string) => void;
};

type MoveTemplateModalState = {
    superId: string;
};

export default class MoveTemplateModal extends React.Component<MoveTemplateModalProps, MoveTemplateModalState> {
    public static readonly type = 'move-template';

    constructor(props: MoveTemplateModalProps) {
        super(props);
        this.state = {
            superId: this.props.superId
        };
    }

    render() {
        return <>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <label htmlFor="templateSuperIdSelect">Super ID:</label>
                <TemplateSelector
                    id="templateSuperIdSelect"
                    showNone={false}
                    templates={this.props.templates}
                    onChange={this.onSuperIdSelectChange}
                    value={this.state.superId}
                />
            </div>
            <button onClick={this.onMoveButtonClick}>Move</button>
        </>;
    }

    onSuperIdSelectChange: React.ChangeEventHandler<HTMLSelectElement> = (event) => {
        this.setState({ superId: event.currentTarget.value });
    }

    onMoveButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        const templates = this.props.templates;
        const template = templates.find(template => template.id === this.props.templateId);
        const superId = this.state.superId;
        if (!templates.find(template => template.id === superId)) {
            alert(`A template with ID ${this.props.superId} does not exist.`);
            return;
        }

        this.props.onConfirm(this.props.templateId, superId);
    }
}
