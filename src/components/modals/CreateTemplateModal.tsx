import * as React from 'react';
import TemplateSelector from '../TemplateSelector';
import { once, daily, weekly, monthly, dailyDependent } from '../../iterationLogics';
import { done, note, yesNo } from '../../resolutionLogics';

export type CreateTemplateModalProps = {
    templates: Template[];
    onConfirm: (template: Template) => void;
};

type CreateTemplateModalState = {
    id: string;
    title: string;
    superId: string;
    iterationLogic: string;
    resolutionLogic: string;
};

export default class CreateTemplateModal extends React.Component<CreateTemplateModalProps, CreateTemplateModalState> {
    public static readonly type = 'create-template';

    constructor(props: CreateTemplateModalProps) {
        super(props);
        this.state = {
            id: '',
            title: '',
            superId: '',
            iterationLogic: '',
            resolutionLogic: '',
        };
    }

    render() {
        return (
            <>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <label htmlFor="templateIdInput">ID:</label>
                    <input id="templateIdInput" onChange={this.onIdInputChange} value={this.state.id} />
                </div>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <label htmlFor="templateTitleInput">Title:</label>
                    <input id="templateTitleInput" onChange={this.onTitleInputChange} value={this.state.title} />
                </div>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <label htmlFor="templateSuperIdSelect">Super ID:</label>
                    <TemplateSelector
                        id="templateSuperIdSelect"
                        showNone={true}
                        templates={this.props.templates}
                        onChange={this.onSuperIdSelectChange}
                        value={this.state.superId}
                    />
                </div>
                {this.state.superId === '' && <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <label htmlFor="templateIterationLogicTextArea">Instance Logic:</label>
                    <textarea id="templateIterationLogicTextArea" onChange={this.onIterationLogicTextAreaChange} value={this.state.iterationLogic} />
                    <div>
                        <button data-key="once" onClick={this.onLoadIterationLogicButtonClick}>Once</button>
                        <button data-key="daily" onClick={this.onLoadIterationLogicButtonClick}>Daily</button>
                        <button data-key="weekly" onClick={this.onLoadIterationLogicButtonClick}>Weekly</button>
                        <button data-key="monthly" onClick={this.onLoadIterationLogicButtonClick}>Monthly</button>
                    </div>
                </div>}
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <label htmlFor="templateResolutionLogicTextArea">Resolution Logic:</label>
                    <textarea id="templateResolutionLogicTextArea" onChange={this.onResolutionLogicTextAreaChange} value={this.state.resolutionLogic} />
                    <div>
                        <button data-key="done" onClick={this.onLoadResolutionLogicButtonClick}>Done</button>
                        <button data-key="note" onClick={this.onLoadResolutionLogicButtonClick}>Note</button>
                        <button data-key="yesNo" onClick={this.onLoadResolutionLogicButtonClick}>Yes/No</button>
                    </div>
                </div>
                <button onClick={this.onCreateButtonClick}>Create</button>
            </>
        );
    }

    onIdInputChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        this.setState({ id: event.currentTarget.value });
    }

    onTitleInputChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        this.setState({ title: event.currentTarget.value });
    }

    onSuperIdSelectChange: React.ChangeEventHandler<HTMLSelectElement> = (event) => {
        this.setState({ superId: event.currentTarget.value });
    }

    onIterationLogicTextAreaChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
        this.setState({ iterationLogic: event.currentTarget.value });
    }

    onResolutionLogicTextAreaChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
        this.setState({ resolutionLogic: event.currentTarget.value });
    }

    onLoadIterationLogicButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        switch (event.currentTarget.dataset['key']) {
            case 'once': {
                const id = (this.props.templates.reduce((id, template) => Math.max(Number(template.id) || 0, id), 0) + 1).toString();
                this.setState({ iterationLogic: once(id) });
                break;
            }
            case 'daily': this.setState({ iterationLogic: daily }); break;
            case 'weekly': this.setState({ iterationLogic: weekly }); break;
            case 'monthly': this.setState({ iterationLogic: monthly }); break;
            default: throw new Error(`Unexpected key ${event.currentTarget.dataset['key']}`);
        }
    }

    onLoadResolutionLogicButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        switch (event.currentTarget.dataset['key']) {
            case 'done': this.setState({ resolutionLogic: done }); break;
            case 'note': this.setState({ resolutionLogic: note }); break;
            case 'yesNo': this.setState({ resolutionLogic: yesNo }); break;
            default: throw new Error(`Unexpected key ${event.currentTarget.dataset['key']}`);
        }
    }

    onCreateButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        const templates = this.props.templates;

        let id = this.state.id.trim();
        if (id !== '') {
            const template = templates.find(template => template.id === id);
            if (template) {
                alert(`A template with ID ${id} (${template.title}) already exists.`);
                return;
            }
        } else {
            id = (templates.reduce((id, template) => Math.max(Number(template.id) || 0, id), 0) + 1).toString();
        }

        const title = this.state.title.trim();
        if (title === '') {
            alert('Title must be filled in');
            return;
        }

        const superId = this.state.superId || null;
        if (superId !== null && !templates.find(template => template.id === superId)) {
            alert(`A template with ID ${id} does not exist.`);
            return;
        }

        const iterationLogic = superId === null ? this.state.iterationLogic : dailyDependent;
        const resolutionLogic = this.state.resolutionLogic;

        this.props.onConfirm({ id, title, superId, iterationLogic, resolutionLogic });
    }
}
