import * as React from 'react';

type ModalProps = {
    title: string;
    onCancel: () => void;
};

export default class Modal extends React.Component<ModalProps, {}> {
    private static readonly OuterDivStyle: React.CSSProperties = { background: 'gray', display: 'flex', height: '100%', justifyContent: 'center', left: 0, overflow: 'auto', position: 'fixed', top: 0, width: '100%', };
    private static readonly InnerDivStyle: React.CSSProperties = { padding: '0 0 50vw 0', width: '50%' };
    render() {
        return (
            <div style={Modal.OuterDivStyle}>
                <div style={Modal.InnerDivStyle}>
                    <div style={{ alignItems: 'center', display: 'flex' }}>
                        <h2 style={{ flex: 1 }}>{this.props.title}</h2>
                        <button onClick={this.props.onCancel}>Cancel</button>
                    </div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
