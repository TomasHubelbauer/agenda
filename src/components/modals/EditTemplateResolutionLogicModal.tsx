import * as React from 'react';

export type EditTemplateResolutionLogicModalProps = {
    templateId: string;
    resolutionLogic: string;
    onConfirm: (templateId: string, resolutionLogic: string) => void;
};

type EditTemplateResolutionLogicModalState = {
    resolutionLogic: string;
};

export default class EditTemplateResolutionLogicModal extends React.Component<EditTemplateResolutionLogicModalProps, EditTemplateResolutionLogicModalState> {
    public static readonly type = 'edit-template-resolution-logic';
    constructor(props: EditTemplateResolutionLogicModalProps) {
        super(props);
        this.state = { resolutionLogic: this.props.resolutionLogic };
    }

    render() {
        return <>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <label htmlFor="templateResolutionLogicTextArea">Resolution logic:</label>
                <textarea id="templateResolutionLogicTextArea" onChange={this.onTemplateResolutionLogicTextAreaChange} value={this.state.resolutionLogic} />
            </div>
            <button onClick={this.onUpdateButtonClick}>Update</button>
        </>;
    }

    onTemplateResolutionLogicTextAreaChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
        this.setState({ resolutionLogic: event.currentTarget.value });
    }

    onUpdateButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        this.props.onConfirm(this.props.templateId, this.state.resolutionLogic);
    }
}
