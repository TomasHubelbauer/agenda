import * as React from 'react';

export type EditTemplateIterationLogicModalProps = {
    templateId: string;
    iterationLogic: string;
    onConfirm: (templateId: string, iterationLogic: string) => void;
};

type EditTemplateIterationLogicModalState = {
    iterationLogic: string;
};

export default class EditTemplateIterationLogicModal extends React.Component<EditTemplateIterationLogicModalProps, EditTemplateIterationLogicModalState> {
    public static readonly type = 'edit-template-iteration-logic';
    constructor(props: EditTemplateIterationLogicModalProps) {
        super(props);
        this.state = { iterationLogic: this.props.iterationLogic };
    }

    render() {
        return <>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <label htmlFor="templateIterationLogicTextArea">Iteration logic:</label>
                <textarea id="templateIterationLogicTextArea" onChange={this.onTemplateIterationLogicTextAreaChange} value={this.state.iterationLogic} />
            </div>
            <button onClick={this.onUpdateButtonClick}>Update</button>
        </>;
    }

    onTemplateIterationLogicTextAreaChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
        this.setState({ iterationLogic: event.currentTarget.value });
    }

    onUpdateButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        this.props.onConfirm(this.props.templateId, this.state.iterationLogic);
    }
}
