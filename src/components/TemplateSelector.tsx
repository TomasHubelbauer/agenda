import * as React from 'react';

type TemplateSelectorProps = {
  id: string;
  showNone: boolean;
  templates: Template[];
  onChange: React.ChangeEventHandler<HTMLSelectElement>;
  value: string;
};

export default function TemplateSelector({ id, showNone, templates, onChange, value }: TemplateSelectorProps) {
  return (
    <select id={id} onChange={onChange} value={value}>
        {showNone && <option key="" value="">None</option>}
        {templates.sort((a, b) => a.title.localeCompare(b.title)).map(template => (
          <option key={template.id} value={template.id}>{template.title} ({template.id})</option>
        ))}
    </select>
  );
}
