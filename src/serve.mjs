import express from 'express';
import fs from 'fs-extra';

const app = express();
app.get('/version', async (request, response) => {
  const version = ((await fs.readJson('package.json', { throws: false })) || { version: 'unknown' }).version;
  let changelog = await fs.pathExists('CHANGELOG.md') ? String(await fs.readFile('CHANGELOG.md')) : '(no changelog)';
  response.send(version + '<h2>Change log</h2>' + changelog.replace(/\n/g, '<br />'));
});

app.use(express.static('dist'));
app.listen(process.env.PORT || 3000, () => console.log('Listening on port 3000!'));
