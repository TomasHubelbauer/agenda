import store from "./store";
import * as iterationLogics from '../src/iterationLogics';

function* days(fromDate: Date, toDate: Date) {
  let currentDate = new Date(fromDate);
  while (currentDate <= toDate) {
    yield new Date(currentDate);
    currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
  }
}

// TODO: Rename to skeletons/principals?
function* instances(templateId: string, fromDate: Date, toDate: Date) {
  const template = store.templates.find(template => template.id === templateId);
  if (template === undefined) {
    throw new Error(`Template with ID ${templateId} was not found.`);
  }

  yield* decorateSkeletons(template, fromDate, toDate);
}

export function iterateSkeletons(template: Template, /* eval: */ fromDate: Date, toDate: Date) {
  const { once, ...logics } = iterationLogics;
  // TODO: Figure out what to do with once here type-wise.
  const iterationLogic = (logics as any as ({ [logic: string]: string | undefined; }))[template.iterationLogic] || template.iterationLogic;
  return eval(`(function* logic() { ${iterationLogic} })`)() as IterableIterator<Skeleton>;
}

function* validateSkeletons(template: Template, fromDate: Date, toDate: Date) {
  const ids: string[] = [];
  for (const skeleton of iterateSkeletons(template, fromDate, toDate)) {
    if (skeleton.id === null || skeleton.id === undefined || typeof skeleton.id !== 'string') {
      throw new Error('Skeleton ID is not given or not a string.');
    }

    if (ids.includes(skeleton.id)) {
      alert(`Template ${template.title} (${template.id}) generated two skeleton with the same ID ${skeleton.id}.`);
      continue;
    }

    if (skeleton.superId !== undefined && skeleton.superId !== null && typeof skeleton.superId !== 'string') {
      throw new Error(`Skeleton super ID ${skeleton.superId} is not undefined, null or not a string.`);
    }

    if (skeleton.title !== undefined && (skeleton.title === null || typeof skeleton.title !== 'string')) {
      throw new Error(`Skeleton title ${skeleton.title} is not undefined or not a string (must not be null).`);
    }

    ids.push(skeleton.id);
    yield skeleton;
  }
}

function* decorateSkeletons(template: Template, fromDate: Date, toDate: Date) {
  for (const skeleton of validateSkeletons(template, fromDate, toDate)) {
    skeleton.id = template.id + '-' + skeleton.id;
    yield skeleton;
  }
}

export function* generateInstances(template: Template, fromDate: Date, toDate: Date) {
  for (const skeleton of decorateSkeletons(template, fromDate, toDate)) {
    // Ignore skeletons with backing resolutions.
    if (!store.resolutions.find(i => i.id === skeleton.id)) {
      const { id, superId, title, ...check } = skeleton;
      if (Object.keys(check).length > 0) {
        throw new Error(`Generator provided a skeleton with unexpected keys: ${Object.keys(check).join(', ')}`);
      }

      const instance: Instance = {
        id: skeleton.id,
        templateId: template.id,
        superId: skeleton.superId || null,
        title: skeleton.title || template.title,
      };

      yield instance;
    }
  }
}

// TODO: Consider prior which is a timeout so there can be a flow of A -> wait -> can do B now? Maybe timeout is another primitive if one is introduced for questions/decisions.
// TODO: Consider allowing/displaying an item only if time allows etc., probably also a prior/condition
// TODO: Consider introducing priorIds which would be a set of ids that must be done before this item can be yielded (for cases where multiple items must be done to enable an item, like `gd`)
// TODO: Consider that right now superId is take from template ID so that means it's locked to current date, when time sliding is introduced, this will break so much you don't even

export function* flow(items: InstanceOrResolution[], templates: Template[], superId: string | null = null, ...pathItems: Instance[]): IterableIterator<Instance[]> {
  let hasSubitems = false;
  for (const item of items) {
    if (item.id === superId) {
      continue;
    }

    if ((superId === null && item.superId === null) || (item.superId !== null && item.superId === superId)) {
      if (item.resolution !== undefined && item.resolution !== null) {
        // Keep inspecting dependants of resolutions (otherwise completing and instance would obscure all its dependants), but skip them to raise their dependants' level.
        yield* flow(items, templates, item.id, ...pathItems);
      } else if (item.title !== undefined) {
        yield* flow(items, templates, item.id, ...pathItems, item as Instance); // TODO: Work out if I can help TS understand the guaranteed in the condition.
      } else {
        throw new Error('Invalid object shape.');
      }

      hasSubitems = true;
    }
  }

  // Prevent incomplete paths from being returned.
  if (!hasSubitems) {
    yield pathItems;
  }
}
