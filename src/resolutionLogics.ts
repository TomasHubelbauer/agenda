export const done = `true`.trim();

export const note = `prompt('Note:')`.trim();

export const yesNo = `confirm('Yes or no?')`.trim();
