import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App';
import store from './store';
import { once, daily, dailyDependent, weekly, monthly } from './iterationLogics';

window.addEventListener('load', async () => {
    ReactDOM.render(<App />, document.querySelector<HTMLDivElement>('#appDiv'));
});

window.addEventListener('pageshow', event => {
    // Refresh upon re-entering the tab in case local storage was changed in another instance.
    ReactDOM.render(<App />, document.querySelector<HTMLDivElement>('#appDiv'));
});

document.addEventListener('visibilitychange', event => {
    if (document.visibilityState === 'visible') {
        // Refresh upon re-entering the tab in case local storage was changed in another instance.
        ReactDOM.render(<App />, document.querySelector<HTMLDivElement>('#appDiv'));
    }
});
