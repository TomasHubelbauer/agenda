// TODO: Change this so that each entry has a local store record and there is a book-keeping record like:
// agenda-template-a {}
// agenda-template-1 {}
// agenda-templates ['a', '1']
// Same for instances…
// After each operations, localStorage.keys is checked to ensure it is consistent with the values of the templates and instance array
export default class Store {
  public static get templates() {
    const templates = localStorage.getItem('agenda-templates');
    if (templates === null) {
        localStorage.setItem('agenda-templates', '[]');
        return [];
    }

    return JSON.parse(templates) as Template[];
  }

  public static set templates(templates: Template[]) {
    localStorage.setItem('agenda-templates', JSON.stringify(templates));
  }

  public static get resolutions() {
    const resolutions = localStorage.getItem('agenda-instances'); // TODO: Migrate
    if (resolutions === null) {
        localStorage.setItem('agenda-instances', '[]'); // TODO: Migrate
        return [];
    }

    return JSON.parse(resolutions) as Resolution[];
  }

  public static set resolutions(resolutions: Resolution[]) {
    localStorage.setItem('agenda-instances', JSON.stringify(resolutions)); // TODO: Migrate
  }
}
