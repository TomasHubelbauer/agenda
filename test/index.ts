import { generateInstances, flow } from '../src/logic';
import { done } from '../src/resolutionLogics';
import store from '../src/store';

const items: { [key: string]: string | undefined; } = {};
(global as any).localStorage = {
  getItem: (key: string) => items[key] || null,
  setItem: (key: string, value: string) => items[key] = value,
};

const principalTemplate: Template = {
  id: 'gu',
  superId: null,
  title: 'Gu',
  iterationLogic: 'daily',
  resolutionLogic: done,
};

const dependentTemplate1: Template = {
  id: 'otw',
  superId: 'gu',
  title: 'Otw',
  iterationLogic: 'dailyDependent',
  resolutionLogic: done,
};

const dependentTemplate2: Template = {
  id: 'ctw',
  superId: 'otw',
  title: 'Ctw',
  iterationLogic: 'dailyDependent',
  resolutionLogic: done,
};

store.templates = [ principalTemplate, dependentTemplate1, dependentTemplate2 ];

//store.resolutions = [];
// gu-2018-04-28 -> gu-2018-04-29 -> otw-gu-2018-04-29 -> ctw-otw-gu-2018-04-29
// gu-2018-04-28 -> otw-gu-2018-04-28 -> ctw-otw-gu-2018-04-28

//store.resolutions = [ { id: 'gu-2018-04-28', templateId: 'gu', superId: null, resolution: true } ];
// gu-2018-04-29 -> otw-gu-2018-04-29 -> ctw-opt-gu-2018-04-29
// otw-gu-2018-04-28 -> ctw-otw-gu-2018-04-28

store.resolutions = [
  { id: 'gu-2018-04-28', templateId: 'gu', superId: null, resolution: true },
  { id: 'otw-gu-2018-04-28', templateId: 'otw', superId: 'gu', resolution: true },
];
// gu-2018-04-29 -> otw-gu-2018-04-29 -> ctw-opt-gu-2018-04-29
// ctw-otw-gu-2018-04-28

const fromDate = new Date(new Date().setDate(new Date().getDate() - 1));
const toDate = new Date(new Date().setDate(new Date().getDate()));

void function() {
  console.log('Instances:');
  for (const template of store.templates) {
    console.log('\t', template.title, '|', template.superId, '->', template.id);
    for (const instance of generateInstances(template, fromDate, toDate)) {
      console.log('\t\t', instance.title, '|', instance.superId, '->', instance.id);
    }
  }

  console.log('Resolutions:');
  for (const resolution of store.resolutions) {
    console.log('\t', resolution.superId, '->', resolution.id);
  }

  console.log();
  const resolutions = store.resolutions;
  const instances = store.templates.reduce((instances, template) => [...instances, ...generateInstances(template, fromDate, toDate)], [] as Instance[]);
  const paths = [ ...flow([ ...instances, ...resolutions ], store.templates) ];
  for (const path of paths) {
    console.log(path.map(instance => instance.id).join(' -> '));
  }
}()
