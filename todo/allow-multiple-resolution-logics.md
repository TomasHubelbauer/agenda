# Allow multiple resolution logics

So that an item can have things like *Done*, *Skipped*, *Blocked* etc.
But still be logics (as opposed to for example an array of strings) so interactive resolutions are still possible and flexible.
