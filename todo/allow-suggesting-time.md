# Allow suggesting time

Dependent items can provide an offset from independent items at which they should appear. It will resolve relatively to the independent item.

Independent items should provide absolute time value or `undefined` in which their order is used.
