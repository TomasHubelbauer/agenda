# Agenda

> An app for navigating habits and errands.

## Installing

- User: [agenda.hubelbauer.net](https://agenda.hubelbauer.net/)
- Developer: `yarn`

## Running

`yarn dev`

Or to simulate live built site:

```wsl
yarn start
```

## Publishing

- Update changelog
- Update `version` in `package.json`
- Deploy

## Deploying

```sh
wsl
git push dokku master
```

## Contributing

- Use [React DevTools](https://fb.me/react-devtools)

See [todo](todo).
